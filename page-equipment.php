<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section id="equipment">
	
	<div class="row">
		<div class="block-page-intro">
			<h5 class="block-page-intro-secondary-title"><?php the_field('equipment_page_intro_title'); ?></h5>
			<?php the_field('equipment_page_intro'); ?>
		</div>
	</div>

<?php

	$equipmentCats = get_terms( 'equipment_type', array(
		'hide_empty' => true,
		'orderby' => 'menu_order'
	));
	$equipmentCatCount = 0;

	if (!empty($equipmentCats)) {
?>
	<div id="equipment-tabs-container">
		<ul class="tabs" data-tabs id="equipment-tabs">
<?php
	    foreach ( $equipmentCats as $equipmentCat ) {
	    	$equipmentCatCount++;
?>
			<li class="tabs-title <?php if($equipmentCatCount == 1): ?>is-active<?php endif; ?>">
				<a href="#<?php echo $equipmentCat->slug; ?>" aria-selected="true">
					<img src="<?php the_field('equipment_type_icon', $equipmentCat); ?>"/>
					<span><?php echo $equipmentCat->name; ?></span>
				</a>
			</li>
<?php
	    }
?>
		</ul>
	</div>
<?php
	}
?>	

<?php

	if (!empty($equipmentCats)) {

	$equipmentCatCount = 0;

?>
	<div class="row">
		<div class="tabs-content" id="equipment-tabs-content-container" data-tabs-content="equipment-tabs">

<?php
	    foreach ( $equipmentCats as $equipmentCat ) {
	    	$equipmentCatCount++;
?>

			<div class="tabs-panel equipment-panel <?php if($equipmentCatCount == 1): ?>is-active<?php endif; ?>" id="<?php echo $equipmentCat->slug; ?>">

				<?php
					
					$equipmentArgs = array(
					    'posts_per_page' => -1,
					    'post_type' => 'equipment_features',
					    'order_by' => 'menu_order',
					    'tax_query' => array(
					        array(
					            'taxonomy' => 'equipment_type',
					            'field' => 'term_id',
					            'terms' => $equipmentCat->term_id
					        )
					    )
					);

					$equipment = new WP_Query($equipmentArgs);
					$equipmentCount = 0;

					if($equipment->have_posts()):
						while($equipment->have_posts()): $equipment->the_post();
						$equipmentCount++;
				?>

				<div class="row content-block-container">
					<?php if($equipmentCount % 2 == 0): ?>
					<div class="large-8 medium-8 columns">
						<div class="v-center">
							<div class="block-title">
								<h5 class="block-secondary-title"><?php the_title(); ?></h5>
							</div>
							<?php the_content(); ?>
							<a class="button" href="<?php the_permalink(); ?>">Learn More</a>
						</div>
					</div>
					<div class="large-4 medium-4 columns">
						<img src="<?php the_post_thumbnail_url(); ?>"/>
					</div>
					<?php else: ?>
					<div class="large-4 medium-4 columns">
						<img src="<?php the_post_thumbnail_url(); ?>"/>
					</div>
					<div class="large-8 medium-8 columns">
						<div class="v-center">
							<div class="block-title">
								<h5 class="block-secondary-title"><?php the_title(); ?></h5>
							</div>
							<?php the_content(); ?>
							<a class="button" href="<?php the_permalink(); ?>">Learn More</a>
						</div>
					</div>
					<?php endif; ?>
				</div>

				<hr/>

				<?php
						endwhile;
					endif;

					wp_reset_postdata();

				?>

			</div>

<?php } ?>

		</div>
	</div>

<?php } ?>

	</div>
</section>

<?php endwhile; ?>

<?php get_template_part('inc/modules/cta-build-your-system'); ?>

<?php get_footer(); ?>