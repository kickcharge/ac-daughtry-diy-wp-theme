<?php get_header(); ?>

			<section id="breadcrumbs">
				<div class="row">
					<h3><?php the_title(); ?></h3>
					<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p>','</p>');
						}
					?>
				</div>
			</section>

		    <section id="primary-content" data-equalizer>
		    	<div class="row ">
		    		<div class="large-7 medium-8 columns" id="column-primary" data-equalizer-watch>

						<?php
							if(have_posts()):
								while(have_posts()): the_post();
						?>
							<?php the_content(); ?>
						<?php
								endwhile;
							endif;
						?>

		    		</div>

		    		<div class="large-4 medium-4 columns" id="column-secondary" data-equalizer-watch>
						<?php get_sidebar(); ?>
		    		</div>
		    	</div>

		    </section>

<?php get_footer(); ?>