<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section class="divider"></section>

<section class="base-styles">
	<div class="row ">
		<div class="column">
			<?php the_content(); ?>
		</div>
	</div>
</section>

<?php get_template_part('inc/modules/cta-benefits'); ?>

<?php get_template_part('inc/modules/cta-how-it-works'); ?>

<?php endwhile; ?>

<?php get_footer(); ?>