jQuery(document).ready(function($){

	$(document).foundation();
	
/*
	var s = skrollr.init({
		mobileCheck: function() {
        	return false;
        }		
	});
*/

	$(document).bind('gform_post_render', function(){
	   offsetFooter();
	});

	var offCanvas = new Foundation.OffCanvas($('#off-canvas-nav'));
	
	function moveText() {
		$('.hero-text', $('.is-active')).foundation('toggle');
	}
	
	$('[data-orbit]').on('slidechange.zf.orbit', moveText);
	
	$(window).resize(function() {
	    if(window.innerWidth > 641) {
		    $('#off-canvas-nav').foundation('close');
	    }
	});

});