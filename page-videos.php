<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section id="videos">

	<div class="row">
		<div class="column">
			<div class="block-title">
				<h5 class="block-secondary-title"><?php the_field('videos_block_intro_title'); ?></h5>
				<?php the_field('videos_block_intro_content'); ?>
			</div>
		</div>
	</div>

<?php if(have_rows('videos')): ?>
	
	<div class="row">
		<?php
			$videosCount = 0;
			while(have_rows('videos')): the_row();
			$videosCount++;
		?>
			
			<div class="video row content-block-container">
				<div class="large-6 medium-6 columns">
					<div class="responsive-embed">
						<?php
							$videoEmbedCode = get_sub_field('video_embed_code');
							preg_match('/src="([^"]+)"/', $videoEmbedCode, $videoEmbedSource);
							$videoEmbedUrl = $videoEmbedSource[1] . '?enablejsapi=1';
						?>
						<iframe width="560" height="315" src="<?php echo $videoEmbedUrl; ?>" frameborder="0" allowfullscreen id="video-<?php echo $videosCount; ?>"></iframe>
					</div>				
				</div>
				<div class="large-6 medium-6 columns">
					<h5><?php the_sub_field('video_title'); ?></h5>
			  		<?php the_sub_field('video_description'); ?>
				</div>
			</div>

			<hr/>

		<?php endwhile; ?>
	</div>

<?php endif; ?>

</section>

<?php endwhile; ?>

<?php get_footer(); ?>

<script type="text/javascript" src="http://www.youtube.com/player_api"></script>
<script type="text/javascript">

	jQuery(window).load(function(){

	    players = new Array();

	    function onYouTubeIframeAPIReady() {
	        var temp = jQuery("iframe", ".responsive-embed");
	        for (var i = 0; i < temp.length; i++) {
	            var t = new YT.Player(jQuery(temp[i]).attr('id'), {
	                events: {
	                    'onStateChange': onPlayerStateChange
	                }
	            });
	            players.push(t);
	        }
	    }

	    onYouTubeIframeAPIReady();

	    function onPlayerStateChange(event) {
	        if (event.data == YT.PlayerState.PLAYING) {
	            var temp = event.target.a.src;
	            var tempPlayers = jQuery("iframe", ".responsive-embed");
	            for (var i = 0; i < players.length; i++) {
	                if (players[i].a.src != temp) 
	                    players[i].stopVideo();
	            }
	        }
	    }

	});

</script>