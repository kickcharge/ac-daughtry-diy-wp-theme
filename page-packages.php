<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section>	

	<div class="row">
		<div class="columns">

			<?php

				$packageArgs = array(
					'post_type' => 'package',
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'posts_per_page' => 3,
					'meta_key' => 'include_in_table',
					'meta_value' => 1
				);

				$packages = new WP_Query($packageArgs);

				$packageFeatures = [];
				$count = 0;

				if($packages->have_posts()):
					while($packages->have_posts()): $packages->the_post();
						
						$count++;

						$package = [];

						$package['id'] = get_the_ID();
						$package['features'] = get_field('package_features');
						$package['best_value'] = get_field('best_value');

						array_push($packageFeatures, $package);

					endwhile;
				endif;

			?>

			<table id="plans-table" class="show-for-medium">
				<tr class="plans-value">
					<td>&nbsp;</td>
					<?php
						if($packages->have_posts()):
							while($packages->have_posts()): $packages->the_post();
					?>

					<?php if(get_field('best_value') == 0): ?>
					<td>&nbsp;</td>
					<?php else: ?>
					<td class="bv">Best Value</td>
					<?php endif; ?>

					<?php
							endwhile;
						endif;
						wp_reset_postdata();
					?>
				</tr>
				<tr class="plans-title">
					<td>
						&nbsp;
					</td>
					<?php
						if($packages->have_posts()):
							while($packages->have_posts()): $packages->the_post();
					?>					
					<td <?php if(get_field('best_value') == 1): ?>class="bv"<?php endif; ?>>
						<img src="<?php the_field('package_icon'); ?>"/>
						<span><?php the_field('formatted_package_title'); ?></span>
					</td>
					<?php
							endwhile;
						endif;
						wp_reset_postdata();
					?>
				</tr>
				<tr class="plans-price">
					<td>
						<span>Package<br/>Features</span>
					</td>
					<?php
						if($packages->have_posts()):
							while($packages->have_posts()): $packages->the_post();
					?>
					<td <?php if(get_field('best_value') == 1): ?>class="bv"<?php endif; ?>>
						<span class="plan-price">$<?php the_field('package_price'); ?></span>
						<span class="plan-price-desc"><?php the_field('package_terms'); ?></span>
						<a href="<?php echo the_permalink(); ?>" class="button alt-grey">View Package</a>
						<a href="<?php echo bloginfo('url'); ?>/shop/" class="button alt-red">Buy Now</a>
					</td>
					<?php
							endwhile;
						endif;
						wp_reset_postdata();
					?>
				</tr>
				<?php
					
					$featuresArgs = array(
						'post_type' => 'features',
						'orderby' => 'menu_order',
						'order' => 'ASC',
						'posts_per_page' => -1
					);

					$features = new WP_Query($featuresArgs);

					if($features->have_posts()):
						while($features->have_posts()): $features->the_post();

				?>
				<tr class="plan-features">
					<td class="has-desc"><?php the_title(); ?> <span><?php the_content(); ?></span></td>

					<?php foreach($packageFeatures as $packageFeature): ?>
						<?php if(in_array(get_the_ID(), $packageFeature['features'])): ?>
						<td <?php if($packageFeature['best_value'] == 1): ?>class="bv"<?php endif; ?>><img src="<?php echo bloginfo('template_directory'); ?>/img/included-in-plan.png" width="30"/></td>
						<?php else: ?>
						<td <?php if($packageFeature['best_value'] == 1): ?>class="bv"<?php endif; ?>>&nbsp;</td>
						<?php endif;?>
					<?php endforeach; ?>

				</tr>
				<?php
						endwhile;
					endif;
					wp_reset_postdata();
				?>
			</table>

		</div>
	</div>

	<?php
		$additionalPackageArgs = array(
			'post_type' => 'package',
			'posts_per_page' => -1,
			'meta_key' => 'include_in_table',
			'meta_value' => 0
		);
		
		$additionalPackages = new WP_Query($additionalPackageArgs);

		if($additionalPackages->have_posts()):
			while($additionalPackages->have_posts()): $additionalPackages->the_post();
	?>
	<div class="row additional-package-cta show-for-medium">
		<div class="column">
			<div class="additional-package-cta-block additional-package-cta-title">
				<span>Basic<br/>Security</span>
			</div>
			<div class="additional-package-cta-block additional-package-cta-icon">
				<img src="<?php the_field('package_icon'); ?>"/>
				<span><?php the_field('formatted_package_title'); ?></span>
			</div>
			<div class="additional-package-cta-block additional-package-cta-price">
				<p><span>$<?php the_field('package_price'); ?></span><?php the_field('package_terms'); ?></p>
			</div>
			<div class="additional-package-cta-block additional-package-cta-btns">
				<a href="<?php the_permalink(); ?>" class="button alt-grey">View Package</a>
				<a href="<?php echo bloginfo('url'); ?>/shop/" class="button alt-red">Buy Now</a>
			</div>
		</div>
	</div>	
	<?php
			endwhile;
		endif;
		wp_reset_postdata();
	?>

	<?php
		$packageArgs = array(
			'post_type' => 'package',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'posts_per_page' => -1
		);

		$packages = new WP_Query($packageArgs);

		if($packages->have_posts()):
	?>
	<div class="row hide-for-medium" id="plans-table-alt">
		<div class="column">
			<?php
				while($packages->have_posts()): $packages->the_post();
			?>
			<div class="plan <?php if(get_field('best_value') == 1): ?>bv<?php endif; ?>">
				<?php if(get_field('best_value') == 1): ?><span class="bv-title">Best Value</span><?php endif; ?>
				<div class="plans-title">
					<?php if(get_field('package_alt_icon')): ?>
						<img src="<?php the_field('package_alt_icon'); ?>" alt="alt-icon"/>
					<?php else: ?>
						<img src="<?php the_field('package_icon'); ?>"/>
					<?php endif; ?>
					<span><?php the_field('formatted_package_title'); ?></span>
				</div>
				<div class="plans-price">
					<span class="plan-price">$<?php the_field('package_price'); ?></span>
					<span class="plan-price-desc"><?php the_field('package_terms'); ?></span>
					<a href="<?php the_permalink(); ?>" class="button alt-grey">View Package</a>
					<a href="<?php echo bloginfo('url'); ?>/shop/" class="button alt-red">Buy Now</a>				
				</div>
			</div>
			<?php
				endwhile;
			?>
		</div>
	</div>
	<?php
		endif;
		wp_reset_postdata();
	?>	

</section>

<?php get_template_part('inc/modules/cta-blocks'); ?>

<?php get_template_part('inc/modules/cta-consultation'); ?>

<?php endwhile; ?>

<?php get_footer(); ?>