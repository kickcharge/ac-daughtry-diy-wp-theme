// Project Notes: 4/7/2017

** Front-end resources **

- The site is built using the Foundation 6 CSS Framework and running Sass, CSS preprocessor. To get that up and running to make any CSS changes please do the following:
Before you can compile, you must run `npm install` and `bower install` inside of theme folder. Finally, run `npm install --global foundation-cli`. Then you will be able to run gulp to compile the SCSS files to the css/app.css file. (You may need to `npm install -g gulp-cli` if it is not already installed).
More information can be found here: http://foundation.zurb.com/sites/docs/installation.html

** Shop Resources **

Shop PHP Files: /inc/gravityforms/
- gforms-functions.php: This file contains global functions used to call data within Gravity Forms fields. There is specific documentation within this file.
- gforms-passdata.php: This file is used to query specific fields within Gravity Forms and includes broken down template files/queries based on which field we are targeting. There is specific documentation within this file.

/inc/gravityforms/queries/
- These individual files are called within the gforms-passdata.php file. Each file has its own WP_Query to pull our Custom Post Type data in to a blank Gravity Forms field. https://codex.wordpress.org/Class_Reference/WP_Query

/inc/gravityforms/templates
- These individual files get called within our gforms-functions.php file. These will pass HTML and JS within our fields. This is where for example we call the post_title of the monitoring plan, and setup the modal window to learn more and call in a description per plan. Please note, I only added sample description content for the first monitoring plan on the form. The field in the WP editor exists for you to populate content for each one and it will automatically populate in to the modal. You will see some JS included within the template files that also call in PHP.

/js/shop.js
- This file contains all standalone javascript for the shopping page. This file is well documented explaining what each component is doing.

All files are included within the themes functions.php file lines #26 (shop.js), #40 (shop.js), #216 (gforms-functions.php) & #217 (gforms-passdata.php)

** WordPress Notes **
- All custom fields are built using the WP Plugin: Advanced Custom Fields (https://www.advancedcustomfields.com/resources/)
- The link above provides excellent documentation on all fields available and how to target them. There are also plenty of resources online. Within their documentation they use `get_field` and `the_field` or variations of it. The data of the fields are stored in the WordPress postmeta table. For the Shop section I used the built in WordPress get_post_meta() function which you will see within the files.
- Gravity Forms documentation can be found here: https://www.gravityhelp.com/documentation/

** Site Resources **
- Within the /resources folder you will find the PSD files as well as the SQL dump of the website as it stands.