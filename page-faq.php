<?php get_header(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<?php while(have_posts()): the_post(); ?>

<?php
	$faqs = get_field('faq');
?>

<section id="faqs-block">
	<div class="row">
		<?php
			foreach($faqs as $i => $faq):
		?>
		<div class="small-12 column">
			<ul class="accordion faq-list" data-accordion data-allow-all-closed="true">
				<li class="accordion-item" data-accordion-item>
					<a href="#" class="accordion-title"><?php echo $faq['question']; ?></a>
					<div class="accordion-content" data-tab-content>
						<?php echo $faq['answer']; ?>
					</div>
				</li>
			</ul>
		</div>
		<?php
			endforeach;
		?>
	</div>
</section>

<?php endwhile; ?>

<?php get_footer(); ?>