<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<?php if(have_rows('guide_group')): ?>
	<?php
		$fileGroupCount = 0;

		while(have_rows('guide_group')): the_row();

		$fileGroupCount++;

	?>
		<section class="guides-block">
			
			<div clas="row">
				<div class="column">
					<div class="block-title">
						<h5 class="block-secondary-title"><?php the_sub_field('guide_group_title'); ?></h5>
						<?php the_sub_field('guide_group_content'); ?>
					</div>					
				</div>
			</div>

			<?php if(have_rows('guide_files')): ?>

			<div class="row">
				<?php while(have_rows('guide_files')): the_row(); ?>
					<div class="large-6 medium-6 columns guide">
						<!-- <img src="<?php the_sub_field('guide_image'); ?>"/> -->
						<h5><?php the_sub_field('guide_title'); ?></h5>
						<?php the_sub_field('guide_description'); ?>
						<a href="<?php the_sub_field('guide_pdf'); ?>" class="button" target="_blank" rel="nofollow">Download</a>
					</div>
				<?php endwhile; ?>
			</div>

			<?php endif; ?>

		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php endwhile; ?>

<?php get_footer(); ?>