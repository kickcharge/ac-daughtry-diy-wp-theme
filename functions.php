<?php

add_action( 'after_setup_theme', 'gds_wp_setup' );

function gds_wp_setup() {

//////////////////////////////////////////////////////////////////
// Register/Enque CSS & JS
//////////////////////////////////////////////////////////////////
function mytheme_enqueue_scripts() {

	// Register Styles
	wp_register_style('theme_style', get_template_directory_uri() . '/css/app.css', array(), 1.0, 'screen');
	wp_register_style('icons', '//s3.amazonaws.com/icomoon.io/69689/ACDaughtrySecuritySystems/style.css?rd3epj', array(), 1.0, 'screen');
	wp_register_style('fonts', '//fast.fonts.net/cssapi/93a23251-28c8-4368-97e8-663d7861d85b.css', array(), 1.0, 'screen');
	wp_register_style( 'remodal-css', get_template_directory_uri() . '/bower_components/remodal/dist/remodal.css', 1.0, 'screen' );
	wp_register_style( 'remodal-theme', get_template_directory_uri() . '/bower_components/remodal/dist/remodal-default-theme.css', 1.0, 'screen' );
	
   	// Register Scripts

	wp_register_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr.js', false, 1, false);
	wp_register_script('foundation', get_template_directory_uri() . '/js/foundation.min.js', false, 1, true);
	wp_register_script('what-input', get_template_directory_uri() . '/js/what-input.min.js', false, 1, true);
	wp_register_script('skrollr', get_template_directory_uri() . '/js/skrollr.min.js', false, 1, true);
	wp_register_script( 'remodal', get_template_directory_uri() . '/bower_components/remodal/dist/remodal.min.js', false, 1, true );
	wp_register_script( 'shop', get_template_directory_uri() . '/js/shop.js', false, 1, true );
	wp_register_script('script', get_template_directory_uri() . '/js/app.js', false, 1, true);

	// Enqueue Styles/Scripts
   	wp_enqueue_style('theme_style');
   	wp_enqueue_style('icons');
   	wp_enqueue_style('fonts');
   	wp_enqueue_style( 'remodal-css' );
   	wp_enqueue_style( 'remodal-theme' );

   	wp_enqueue_script('jquery');
	wp_enqueue_script('foundation', array('jquery'));
	wp_enqueue_script('what-input', array('jquery'));
	wp_enqueue_script( 'remodal', array( 'jquery' ) );
	wp_enqueue_script( 'shop', array( 'jquery' ) );
	wp_enqueue_script('script', array('jquery'));
	wp_enqueue_script('skrollr');

}
add_action('wp_enqueue_scripts', 'mytheme_enqueue_scripts', 1);

//////////////////////////////////////////////////////////////////
// Clean Up WP Head
//////////////////////////////////////////////////////////////////

  remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
  remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
  remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
  remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
  remove_action( 'wp_head', 'index_rel_link' ); // index link
  remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
  remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
  remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
  remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

  // Register Menus

  register_nav_menus(
    array( 'main-menu' => __( 'Main Menu', 'gds_wp' ) )
  );

	// add a favicon to your
	function blog_favicon() {
		echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl').'/favicon.ico" />';
		echo '<link rel="apple-touch-icon" href="'.get_bloginfo('wpurl').'/apple-touch-icon.png" />';
		echo '<link rel="apple-touch-icon" sizes="72x72" href="'.get_bloginfo('wpurl').'/apple-touch-icon-72x72.png" />';
		echo '<link rel="apple-touch-icon" sizes="114x114" href="'.get_bloginfo('wpurl').'/apple-touch-icon-114x114.png" />';

	}
	add_action('wp_head', 'blog_favicon');

//////////////////////////////////////////////////////////////////
// Internationalization Settings - Used for theme translating
//////////////////////////////////////////////////////////////////

	// Change this to your theme text domain, used for internationalising strings
	$theme_text_domain = 'gdstheme';

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
	    'domain'            => $theme_text_domain,           // Text domain - likely want to be the same as your theme.
	    'default_path'      => '',                           // Default absolute path to pre-packaged plugins
	    'parent_menu_slug'  => 'themes.php',         // Default parent menu slug
	    'parent_url_slug'   => 'themes.php',         // Default parent URL slug
	    'menu'              => 'install-required-plugins',   // Menu slug
	    'has_notices'       => true,                         // Show admin notices or not
	    'is_automatic'      => false,            // Automatically activate plugins after installation or not
	    'message'           => '',               // Message to output right before the plugins table
	    'strings'           => array(
	        'page_title'                                => __( 'Install Required Plugins', $theme_text_domain ),
	        'menu_title'                                => __( 'Install Plugins', $theme_text_domain ),
	        'installing'                                => __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
	        'oops'                                      => __( 'Something went wrong with the plugin API.', $theme_text_domain ),
	        'notice_can_install_required'               => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
	        'notice_can_install_recommended'            => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
	        'notice_cannot_install'                     => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
	        'notice_can_activate_required'              => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
	        'notice_can_activate_recommended'           => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
	        'notice_cannot_activate'                    => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
	        'notice_ask_to_update'                      => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
	        'notice_cannot_update'                      => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
	        'install_link'                              => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
	        'activate_link'                             => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
	        'return'                                    => __( 'Return to Required Plugins Installer', $theme_text_domain ),
	        'plugin_activated'                          => __( 'Plugin activated successfully.', $theme_text_domain ),
	        'complete'                                  => __( 'All plugins installed and activated successfully. %s', $theme_text_domain ) // %1$s = dashboard link
	    )
	);

//////////////////////////////////////////////////////////////////
// ACF Customizations
//////////////////////////////////////////////////////////////////

  // Register Options Page

  if(function_exists('acf_add_options_page')) {
  	acf_add_options_page();
  }

} //End gds_wp_setup

//////////////////////////////////////////////////////////////////
// WordPress title tag update: https://codex.wordpress.org/Title_Tag
//////////////////////////////////////////////////////////////////

/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

//////////////////////////////////////////////////////////////////
// Media Settings
//////////////////////////////////////////////////////////////////

add_theme_support( 'post-thumbnails' );

function add_webm_mime_for_upload($mimes) {
	$mimes = array_merge($mimes, array(
		'webm' => 'video/webm',
		'mp4' => 'video/mp4',
		'ogv' => 'video/ogg'
	));
	return $mimes;
}
add_filter('upload_mimes', 'add_webm_mime_for_upload');

//////////////////////////////////////////////////////////////////
// Mixed Content SSL Fix
//////////////////////////////////////////////////////////////////

add_filter('script_loader_src', 'agnostic_script_loader_src', 20,2);
function agnostic_script_loader_src($src, $handle) {
	return preg_replace('/^(http|https):/', '', $src);
}

//////////////////////////////////////////////////////////////////
// Custom Excerpt w/ Variable Length
//////////////////////////////////////////////////////////////////

function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

//////////////////////////////////////////////////////////////////
// Add Custom Shortcodes / CPTs
//////////////////////////////////////////////////////////////////

include_once('inc/shortcodes/shortcodes.php');
/*
include_once('inc/cpt/cpt_case-studies.php');
include_once('inc/cpt/cpt_portfolio.php');
include_once('inc/cpt/cpt_testimonials.php');
include_once('inc/cpt/cpt_coupons.php');
*/

//////////////////////////////////////////////////////////////////
// Add custom logo to admin login screen
//////////////////////////////////////////////////////////////////

function custom_loginlogo() {
	echo '
	<style type="text/css">
		h1 a { background-image: url('.get_stylesheet_directory_uri().'/img/ac-logo.svg) !important; min-width: 300px !important; min-height: 100px !important; margin: 0 auto !important; position: relative !important; z-index: 10000 !important; background-size: auto !important; }
	</style>';
}

add_action('login_head', 'custom_loginlogo');

//////////////////////////////////////////////////////////////////
// Add Custom Functions
//////////////////////////////////////////////////////////////////

add_filter( 'the_content', 'do_shortcode', 11 );
include_once( 'inc/gravityforms/gforms-functions.php' );
include_once( 'inc/gravityforms/gforms-passdata.php' );

// Fix onsubmit issue with validation erros where the error message is blocked by floating nav
add_filter("gform_confirmation_anchor", create_function("","return true;"));

// Fix tab index issue with multiple forms on 1 page
add_filter( 'gform_tabindex', 'gform_tabindexer', 10, 2 );
function gform_tabindexer( $tab_index, $form = false ) {
    $starting_index = 1000; // if you need a higher tabindex, update this number
    if( $form )
        add_filter( 'gform_tabindex_' . $form['id'], 'gform_tabindexer' );
    return GFCommon::$tab_index >= $starting_index ? GFCommon::$tab_index : $starting_index;
}

// Fix issue where on multi-page Gravity Form the page would jump to the bottom when clicking next.
add_filter("gform_confirmation_anchor",
create_function("","return false;"));

function has_children() {

    global $post;

    $children = get_pages( array( 'child_of' => $post->ID ) );
    if( count( $children ) == 0 ) {
        return false;
    } else {
        return true;
    }

}

function foundation_pagination($arrows = true, $ends = true, $pages = 2) {

    if (is_singular()) return;

    global $wp_query, $paged;
    $pagination = '';

    $max_page = $wp_query->max_num_pages;
    if ($max_page == 1) return;
    if (empty($paged)) $paged = 1;

    if ($arrows) $pagination .= foundation_pagination_link($paged - 1, 'arrow' . (($paged <= 1) ? ' unavailable' : ''), '<i class="fa fa-angle-left" aria-hidden="true"></i>', 'Previous Page');
    if ($ends && $paged > $pages + 1) $pagination .= foundation_pagination_link(1);
    if ($ends && $paged > $pages + 2) $pagination .= foundation_pagination_link(1, 'unavailable', '&hellip;');
    for ($i = $paged - $pages; $i <= $paged + $pages; $i++) {
        if ($i > 0 && $i <= $max_page)
            $pagination .= foundation_pagination_link($i, ($i == $paged) ? 'current' : '');
    }
    if ($ends && $paged < $max_page - $pages - 1) $pagination .= foundation_pagination_link($max_page, 'unavailable', '&hellip;');
    if ($ends && $paged < $max_page - $pages) $pagination .= foundation_pagination_link($max_page);

    if ($arrows) $pagination .= foundation_pagination_link($paged + 1, 'arrow' . (($paged >= $max_page) ? ' unavailable' : ''), '<i class="fa fa-angle-right" aria-hidden="true"></i>', 'Next Page');

    $pagination = '<ul class="pagination text-center">' . $pagination . '</ul>';
    $pagination = '<div class="pagination">' . $pagination . '</div>';

    echo $pagination;

}

function foundation_pagination_link($page, $class = '', $content = '', $title = '') {

    $id = sanitize_title_with_dashes('pagination-page-' . $page . ' ' . $class);
    $href = (strrpos($class, 'unavailable') === false && strrpos($class, 'current') === false) ? get_pagenum_link($page) : "#$id";
    $class = empty($class) ? $class : " class=\"$class\"";
    $content = !empty($content) ? $content : $page;
    $title = !empty($title) ? $title : 'Page ' . $page;
    $paged = $content;

    return "<li$class><a id=\"$id\" href=\"$href\" title=\"$title\">$paged</a></li>\n";

}

if ( ! class_exists( 'Foundationpress_Top_Bar_Walker' ) ) :
class Foundationpress_Top_Bar_Walker extends Walker_Nav_Menu {

  function start_lvl( &$output, $depth = 0, $args = array() ) {

    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"menu\">\n";

  }

  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $classes[] = 'menu-item-' . $item->ID;
    $classes[] = 'level-' . $depth;

    $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';


    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $class_names .'>';

    $atts = array();
    $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
    $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
    $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
    $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

    $attributes = '';
    foreach ( $atts as $attr => $value ) {
        if ( ! empty( $value ) ) {
            $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
            $attributes .= ' ' . $attr . '="' . $value . '"';
        }
    }

    $title = apply_filters( 'the_title', $item->title, $item->ID );

    $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . $title . $args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;


    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

  }

}
endif;

if ( ! class_exists( 'Foundationpress_Accordion_Menu_Walker' ) ) :
class Foundationpress_Accordion_Menu_Walker extends Walker_Nav_Menu {

  function start_lvl( &$output, $depth = 0, $args = array() ) {

    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"menu vertical nested\">\n";

  }

  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $classes[] = 'menu-item-' . $item->ID;
    $classes[] = 'level-' . $depth;

    $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';


    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $class_names .'>';

    $atts = array();
    $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
    $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
    $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
    $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

    $attributes = '';
    foreach ( $atts as $attr => $value ) {
        if ( ! empty( $value ) ) {
            $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
            $attributes .= ' ' . $attr . '="' . $value . '"';
        }
    }

    $title = apply_filters( 'the_title', $item->title, $item->ID );

    $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . $title . $args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;


    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

  }

}
endif;

add_filter('body_class','body_class_section');

function body_class_section($classes) {
    global $wpdb, $post;
    if (is_page()) {
        if ($post->post_parent) {
            $parent = end(get_post_ancestors($current_page_id));
        } else {
            $parent = $post->ID;
        }
        $post_data = get_post($parent, ARRAY_A);
        $classes[] = 'parent-' . $post_data['post_name'];
    }
    return $classes;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function get_id_by_slug($page_slug) {
  $page = get_page_by_path($page_slug);
  if ($page) {
    return $page->ID;
  } else {
    return null;
  }
}

?>