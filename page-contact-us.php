<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section>
	<div class="row">
		<div class="columns">
			<div class="block-page-intro">
				<h5 class="block-page-intro-secondary-title"><?php the_field('contact_page_intro_text'); ?></h5>
				<?php the_field('contact_page_intro_sub_text'); ?>
				<a href="tel:9733353931" class="phone-large"><?php the_field('phone_number', 'option'); ?></a>
			</div>

			<?php the_content(); ?>

		</div>
	</div>
</section>

<section id="location-map">
	<div class="row">
		<div class="large-8 medium-8 columns">
			<img src="<?php echo bloginfo('template_directory'); ?>/img/location.jpg"/>
		</div>
		<div class="large-4 medium-4 columns">
			<div class="v-center">
				<div class="block-title">
					<h5 class="block-secondary-title">Our Location</h5>
				</div>
				<?php the_field('address', 'option'); ?>
				<a class="button" href="https://maps.google.com/maps?client=safari&rls=en&oe=UTF-8&q=381+Main+Rd+Montville,+NJ+07045&um=1&ie=UTF-8&sa=X&ved=0ahUKEwj2sriG4_vRAhVl4IMKHaiaDlIQ_AUICCgB" target="_blank" rel="nofollow">Get Directions</a>
			</div>
		</div>		
	</div>
</section>

<?php endwhile; ?>

<?php get_footer(); ?>