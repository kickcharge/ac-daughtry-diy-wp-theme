				<footer>
					<section id="footer-nav">
						<div class="row">
							<a href="<?php bloginfo('url'); ?>/equipment/">Equipment</a>
							<a href="<?php bloginfo('url'); ?>/packages/">Packages</a>
							<a href="<?php bloginfo('url'); ?>/tips/">Tips</a>
							<a href="<?php bloginfo('url'); ?>/support/">Support</a>
							<a href="<?php bloginfo('url'); ?>/company/">Company</a>
						</div>
					</section>

					<section id="footer-contact">
						<div class="row">
							<div class="large-4 medium-4 small-12 medium-offset-1 large-offset-1 columns">
								<h4>A Wise Choice <br/>for Security.</h4>
							</div>
							<div class="large-5 medium-5 small-12 medium-offset-1 large-offset-1 columns">
								<div class="row">
									<div class="large-4 medium-8 columns">
										<address><?php the_field('address', 'option'); ?></address>
									</div>
									<div class="large-4 medium-8 columns">
										<p><span class="icon-phone"></span> <?php the_field('phone_number', 'option'); ?><br/><span class="icon-fax"></span> <?php the_field('fax_number', 'option'); ?></p>
									</div>
									<div class="large-4 medium-4 columns social-links">
										<p>
											<a href="<?php the_field('facebook_url', 'option'); ?>" target="_blank" rel="nofollow"><span class="icon-facebook"></span></a>
											<br/>
											<a href="<?php the_field('twitter_url', 'option'); ?>" target="_blank" rel="nofollow"><span class="icon-twitter"></span></a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</section>

					<section id="footer-copyright">
						<div class="row">
							<div class="columns">
								<p>&copy; <?php echo date('Y'); ?> A.C. Daughtry Security Systems  <span>&bull;</span>  <a href="<?php bloginfo('url'); ?>/privacy-policy/">Privacy Policy</a>  <span>&bull;</span>  <a href="<?php bloginfo('url'); ?>/terms-conditions/">Terms &amp; Conditions</a></p>
								<p>Site by <a href="http://www.graphicd-signs.com/" target="_blank" rel="nofollow">Graphic D-Signs</a></p>
							</div>
						</div>
					</section>

				</footer>

			</div>

		</div>
	</div>

	<?php wp_footer(); ?>

  </body>
</html>