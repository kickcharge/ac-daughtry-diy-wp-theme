<?php get_header(); ?>

<section id="hero">
	<div class="row">
		<div class="hero-text">
			<h3 class="hero-text-primary-text"><?php the_field('hero_primary_text'); ?></h3>
			<h4 class="hero-text-secondary-text"><?php the_field('hero_secondary_text'); ?></h4>
			<a class="button" href="<?php the_field('hero_button_url'); ?>"><span class="icon-owl"></span> <?php the_field('hero_button_text'); ?></a>
		</div>
		<div class="hero-graphic"></div>
	</div>
</section>

<section id="how-it-works">
	<div class="row">
		<div class="columns">
			<div class="home-block-title">
				<h4 class="home-block-primary-title"><?php the_field('how_it_works_primary_text'); ?></h4>
				<h5 class="home-block-secondary-title"><?php the_field('how_it_works_secondary_text'); ?></h5>
			</div>
			
			<?php the_field('how_it_works_content'); ?>
			
			<img class="show-for-medium" src="<?php the_field('how_it_works_image'); ?>"/>
			<img class="hide-for-medium" src="<?php the_field('how_it_works_small_screen_image'); ?>"/>
			
			<a class="button" href="<?php the_field('how_it_works_button_url'); ?>"><?php the_field('how_it_works_button_text'); ?></a>
		</div>
	</div>
</section>

<section id="benefits">
	<div class="row">
		<div class="large-6 medium-6 columns">
			<img src="<?php the_field('benefits_image'); ?>"/>
		</div>
		<div class="large-6 medium-6 columns">
			<div class="home-block-content">
				<div class="home-block-title">
					<h4 class="home-block-primary-title"><?php the_field('benefits_primary_text'); ?></h4>
					<h5 class="home-block-secondary-title"><?php the_field('benefits_secondary_text'); ?></h5>
				</div>
				
				<?php the_field('benefits_content'); ?>
				
				<a class="button" href="<?php the_field('benefits_button_url'); ?>"><?php the_field('benefits_button_text'); ?></a>
			</div>
		</div>			
	</div>
</section>

<section id="overview">
	<div class="row">
		<div class="large-6 medium-6 columns hide-for-medium">
			<img src="<?php the_field('equipment_image'); ?>"/>
		</div>	
		<div class="large-6 medium-6 columns">
			<div class="home-block-content">
				<div class="home-block-title">
					<h4 class="home-block-primary-title"><?php the_field('equipment_primary_text'); ?></h4>
					<h5 class="home-block-secondary-title"><?php the_field('equipment_secondary_text'); ?></h5>
				</div>
				
				<?php the_field('equipment_content'); ?>
				
				<a class="button" href="<?php the_field('equipment_button_url'); ?>"><?php the_field('equipment_button_text'); ?></a>
			</div>
		</div>			
		<div class="large-6 medium-6 columns show-for-medium">
			<img src="<?php the_field('equipment_image'); ?>"/>
		</div>			
	</div>
</section>

<?php get_template_part('inc/modules/cta-build-your-system'); ?>

<?php get_footer(); ?>