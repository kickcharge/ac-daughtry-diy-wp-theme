<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section class="divider"></section>

<?php get_template_part('inc/modules/cta-blocks'); ?>

<?php get_template_part('inc/modules/cta-build-your-system'); ?>

<?php get_template_part('inc/modules/cta-full-service'); ?>

<?php get_template_part('inc/modules/cta-support'); ?>

<?php endwhile; ?>

<?php get_footer(); ?>