= Graphic D-Signs =

* by the Graphic D-Signs team, http://graphicd-signs.com/

== Sass Structure Recommendations ==

* Compilation Instructions (from command line)

sass —-watch main.scss:../main.css —-style compressed

* The structure is meant to make edits easy in the long run by segmenting bits of the site for easy location.

—sass
—-main.scss (global include for all subsequent sass files)
—-layout (header, footer, sidebar, primary content)
—-mixins (functions, helpers, mixins)
—-modules (carousel, galleries, modular components)
—-pages (dedicated page styles typically nested inside body.page-name{} )