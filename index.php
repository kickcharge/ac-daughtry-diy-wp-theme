<?php get_header(); ?>

	<section id="breadcrumbs">
		<div class="row">
			<h3>Blog</h3>
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p>','</p>');
				}
			?>
		</div>
	</section>

	<?php global $wp_query; ?>

		<section id="blog-feed">
			<div class="row block-title">
				<h3>The Security Scoop</h3>
				<h2>A.C. Daughtry blog for news, tips &amp; more.</h2>
			</div>
			<div class="row" id="blog-posts">
				<div class="medium-12 large-6 columns post-column-first">
					<?php
						if(have_posts()):
							while(have_posts()): the_post();
								if($wp_query->current_post % 2 == 0):
					?>
					<article>
						<?php
							if(has_post_thumbnail()):
						?>
						<img src="<?php the_post_thumbnail_url('full'); ?>"/>
						<?php
							else:
						?>
						<img src="<?php echo bloginfo('template_directory'); ?>/img/ac-daughtry-default-featured-img.jpg"/>
						<?php
							endif;
						?>
						<div class="post-excerpt">
							<ul class="post-meta">
								<li><span class="icon-owl"></span> AC Daughtry</li>
								<li><span class="icon-date"></span> <?php the_time('F j, Y'); ?></li>
							</ul>
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<span class="post-title-sep">...</span>
							<p><?php the_excerpt(); ?></p>
							<a href="<?php the_permalink(); ?>" class="button">Read More</a>
						</div>
					</article>
					<?php
								endif;
							endwhile;
						endif;

						wp_reset_postdata();

					?>
				</div>
				<div class="medium-12 large-6 columns post-column-second">
					<?php
						if(have_posts()):
							while(have_posts()): the_post();
								if($wp_query->current_post % 2 !== 0):
					?>
					<article>
						<?php
							if(has_post_thumbnail()):
						?>
						<img src="<?php the_post_thumbnail_url('full'); ?>"/>
						<?php
							else:
						?>
						<img src="<?php echo bloginfo('template_directory'); ?>/img/ac-daughtry-default-featured-img.jpg"/>
						<?php
							endif;
						?>
						<div class="post-excerpt">
							<ul class="post-meta">
								<li><span class="icon-owl"></span> AC Daughtry</li>
								<li><span class="icon-date"></span> <?php the_time('F j, Y'); ?></li>
							</ul>
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<span class="post-title-sep">...</span>
							<p><?php the_excerpt(); ?></p>
							<a href="<?php the_permalink(); ?>" class="button">Read More</a>
						</div>
					</article>
					<?php
								endif;
							endwhile;
						endif;

						wp_reset_postdata();

					?>
				</div>
			</div>
			<div class"row" id="post-pagination">
				<?php foundation_pagination(); ?>
			</div>
		</section>

<?php get_footer(); ?>