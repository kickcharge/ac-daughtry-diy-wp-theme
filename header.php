<!DOCTYPE html>
	<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class( '' ); ?>>

	<header class="container">
		<div class="row large-12 columns top-bar">

			<div class="top-bar-left">

				<h1 id="ac-logo"><a href="<?php bloginfo('url'); ?>">AC Daughtry Security Services</a></h1>

				<div class="primary-menu">
					<div class="user-links">
						<p><a href="#">Login</a> | <a href="<?php echo bloginfo('url'); ?>/shop/">Cart</a></p>
					</div>
					
					<?php
						wp_nav_menu(
							array(
								'container' => false,
								'menu'  => 'Primary Menu',
								'menu_class' => 'dropdown menu',
								'menu_id' => 'primary-nav-container',
								'items_wrap' => '<ul id="%1$s" class="%2$s" data-responsive-menu="accordion medium-dropdown">%3$s</ul>',
								'walker' => new Foundationpress_Top_Bar_Walker
							)
						);
					?>

				</div>

			</div>
			<div class="top-bar-right">
				<a href="<?php echo bloginfo('url'); ?>/shop/" id="raq-btn" class="show-for-medium">Buy Now</a>
				<a href="tel:9733353931" id="call-btn" class="show-for-small-only"><span class="icon-phone"></span></a>
				<a id="off-canvas-trigger" type="button" data-toggle="off-canvas-nav">
					<span class="icon-menu-icon"></span>
					<span class="icon-menu-close-icon"></span>
				</a>
			</div>
		</div>
	</header>

	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

			<div class="off-canvas position-right" data-position="right" id="off-canvas-nav" data-off-canvas>

				<?php
					wp_nav_menu(
						array(
							'container' => false,
							'menu'  => 'Primary Menu',
							'menu_class' => 'vertical menu',
							'menu_id' => 'secondary-nav-container',
							'items_wrap' => '<ul id="%1$s" class="%2$s" data-accordion-menu>%3$s</ul>',
							'walker' => new Foundationpress_Accordion_Menu_Walker
						)
					);
				?>		

			</div>

			<div class="off-canvas-content" data-off-canvas-content>	 