<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<?php get_template_part('inc/modules/cta-faqs'); ?>

<section id="support-docs">
	
	<div class="row">
		<div class="large-6 medium-6 columns">
			<img src="<?php bloginfo('template_directory'); ?>/img/guides-icon.svg"/>
			<div class="block-title">
				<h5 class="block-secondary-title">Guides</h5>
			</div>
			<a href="<?php echo bloginfo('url'); ?>/support/guides/" class="button">View All</a>
		</div>
		<div class="large-6 medium-6 columns">
		<img src="<?php bloginfo('template_directory'); ?>/img/video-icon.svg"/>
			<div class="block-title">
				<h5 class="block-secondary-title">Videos</h5>
			</div>
			<a href="<?php echo bloginfo('url'); ?>/support/videos/" class="button">View All</a>
		</div>			
	</div>

</section>

<?php endwhile; ?>

<?php get_footer(); ?>