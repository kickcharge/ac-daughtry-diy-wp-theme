<?php get_header(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section id="primary-content">
	<div class="row ">
		<div class="large-7 medium-8 columns" id="column-primary">

			<?php
				if(have_posts()):
					while(have_posts()): the_post();
			?>
				<?php the_content(); ?>
			<?php
					endwhile;
				endif;
			?>

		</div>

		<div class="large-4 medium-4 columns" id="column-secondary">
			<?php get_sidebar(); ?>
		</div>
	</div>

</section>

<?php get_footer(); ?>