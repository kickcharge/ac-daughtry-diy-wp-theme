<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section class="divider"></section>

<?php get_template_part('inc/modules/cta-blocks'); ?>

<?php if(have_rows('advantages')): ?>
<section id="plan-advantages">
	<div class="row">

	<?php

		$advantageCount = 12 / count(get_field('advantages'));

		$classes = "large-" . $advantageCount . " medium-" . $advantageCount;

		while(have_rows('advantages')): the_row();

	?>
		<div class="columns <?php echo $classes; ?>">
			<h5><?php the_sub_field('advantage_title'); ?></h5>
			<?php the_sub_field('advantage_content'); ?>
		</div>
	<?php endwhile; ?>

	</div>
</section>
<?php endif; ?>

<?php

$packageEquipmentCount = count(get_field('package_equipment'));
if($packageEquipmentCount):

?>
<section id="plan-features">

	<div class="row">
		<div class="block-title">
			<h5 class="block-secondary-title"><?php the_field('package_features_block_title'); ?></h5>
			<?php the_field('package_features_block_content'); ?>
		</div>
	</div>

	<div class="row">
		<div class="large-6 medium-6 columns">
			<ul>
				<?php
					$packageEquipment = get_field('package_equipment');
					$count = 0;

					foreach($packageEquipment as $equipment) {
					$count++;
				?>
					
					<li><?php echo $equipment->post_title; ?></li>
				
				<?php
					if($count == ($packageEquipmentCount / 2)) {
				?>
			</ul>
		</div>
		<div class="large-6 medium-6 columns">
			<ul>				
				<?php
						}
					}
				?>
			</ul>
		</div>		
	</div>

</section>
<?php endif; ?>

<section id="plan-price">
	<div class="row">
		<div class="columns">
			<div class="block-title">
				<h5 class="block-secondary-title"><?php the_field('package_price_block_title'); ?></h5>
				<?php the_field('package_price_block_content'); ?>
			</div>

			<div class="price">
				<p><span>$<?php the_field('package_price'); ?></span> <?php the_field('package_price_block_terms'); ?></p>
			</div>

			<a href="<?php echo bloginfo('url'); ?>/shop/" class="button">Buy Now</a>
		</div>
	</div>	
</section>

<?php get_template_part('inc/modules/cta-faqs'); ?>

<?php get_template_part('inc/modules/cta-consultation'); ?>

<?php endwhile; ?>

<?php get_footer(); ?>