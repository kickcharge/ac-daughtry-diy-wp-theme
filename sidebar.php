<?php
	global $wp_query;
	$postID = $wp_query->post->ID;
	$postParent = $wp_query->post->post_parent;
?>

<div id="sidebar-container">

	<div class="sidebar-block sidebar-sub-nav">

		<?php if(is_single()): ?>
		<h3>Main Menu</h3>
		<?php elseif(!$postParent): ?>
		<h3><?php the_title(); ?></h3>
		<?php else: ?>
		<h3><?php echo get_the_title($postParent); ?></h3>
		<?php endif; ?>

		<ul>
			<?php

				$excludedPagesArgs = array(
					'posts_per_page' => -1,
					'post_type' => 'page',
					'meta_key' => 'exclude_from_sidebar_navigation',
					'meta_value' => '1'
				);
				$excludedPages = get_posts($excludedPagesArgs);
				$excludedPageIds = array();
				
				foreach($excludedPages as $excludedPage) {
					array_push($excludedPageIds, $excludedPage->ID);
				}
				
				$excludedPageIds = implode(', ', $excludedPageIds);
				
				if(has_children()) {

					wp_list_pages(array( 'title_li' => '', 'child_of' => $postID, 'depth' => 1, 'sort_column' => 'menu_order', 'hierarchical' => 1, 'exclude' => $excludedPageIds));

				} else {

					wp_list_pages(array( 'title_li' => '', 'child_of' => $postParent, 'depth' => 1, 'sort_column' => 'menu_order', 'hierarchical' => 1, 'exclude' => $excludedPageIds));

				}
				
			?>
		</ul>
	</div>

	<?php

		if(get_field('cta')):

		$ctas = get_field('cta');

			foreach($ctas as $ctaID):
	?>

	<div class="sidebar-block sidebar-cta">
		<img src="<?php the_field('image', $ctaID); ?>"/>
		<h4><?php echo get_the_title($ctaID); ?></h4>
		<h3><?php the_field('sub_title', $ctaID); ?></h3>
		<a href="<?php the_field('link_url', $ctaID); ?>" class="button"><?php the_field('link_text', $ctaID); ?></a>
	</div>

	<?php
			endforeach;

		endif;

	?>

</div>

<?php
	wp_reset_postdata();
?>