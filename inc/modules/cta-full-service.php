<section id="full-service">
	<div class="row">
		<div class="columns">
			<div class="block-title">
				<h5 class="block-secondary-title"><?php the_field('full_service_cta_primary_text', 'option'); ?></h5>
				<?php the_field('full_service_cta_content', 'option'); ?>
			</div>

			<a href="http://www.acdsecurity.com/" target="_blank" rel="nofollow" class="button"><?php the_field('full_service_cta_button_text', 'option'); ?></a>

			<h4 class="consultation-cta"><?php the_field('full_service_cta_secondary_text', 'option'); ?></h4>
			<a href="tel:9733353931" class="phone-large"><?php the_field('phone_number', 'option'); ?></a>
		</div>
	</div>	
</section>