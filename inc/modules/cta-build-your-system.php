<section id="shop">
	<div class="row">
		<div class="columns">
			<div class="home-block-title">
				<h4 class="home-block-primary-title"><?php the_field('build_your_system_cta_primary_text', 'option'); ?></h4>
				<h5 class="home-block-secondary-title"><?php the_field('build_your_system_cta_secondary_text', 'option'); ?></h5>
			</div>
			<?php the_field('build_your_system_cta_content', 'option'); ?>
			
			<img src="<?php the_field('build_your_system_cta_image', 'option'); ?>"/>
			
			<a class="button" href="<?php the_field('build_your_system_cta_button_url', 'option'); ?>"><?php the_field('build_your_system_cta_button_text', 'option'); ?></a>
		</div>
	</div>
</section>