<?php
	$pageID = get_id_by_slug('support/faq');
	$faqs = get_field('faq', $pageID);
?>
<section id="faqs-block">
	<div class="row">
		<div class="columns">
			<div class="block-title">
				<h5 class="block-secondary-title">Frequently Asked Questions</h5>
			</div>

			<ul class="accordion faq-list" data-accordion>
				<?php
					foreach($faqs as $i => $faq):
				?>
				<li class="accordion-item" data-accordion-item>
					<a href="#" class="accordion-title"><?php echo $faq['question']; ?></a>
					<div class="accordion-content" data-tab-content>
						<?php echo $faq['answer']; ?>
					</div>
				</li>
				<?php
					endforeach;
				?>
			</ul>
			
			<?php if(!is_page('faq')): ?>
			<a href="<?php echo bloginfo('url'); ?>/support/faq/" class="button">More</a>
			<?php endif; ?>
		</div>
	</div>
</section>