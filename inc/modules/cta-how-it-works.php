<section id="how-it-works">
	<div class="row">
		<div class="columns">
			<div class="home-block-title">
				<h4 class="home-block-primary-title"><?php the_field('how_it_works_cta_primary_text', 'option'); ?></h4>
				<h5 class="home-block-secondary-title"><?php the_field('how_it_works_cta_secondary_text', 'option'); ?></h5>
			</div>
			
			<?php the_field('how_it_works_content', 'option'); ?>
			
			<img class="show-for-medium" src="<?php the_field('how_it_works_cta_image', 'option'); ?>"/>
			<img class="hide-for-medium" src="<?php the_field('how_it_works_cta_small_screen_image', 'option'); ?>"/>
			
			<a class="button" href="<?php the_field('how_it_works_cta_button_url', 'option'); ?>"><?php the_field('how_it_works_cta_button_text', 'option'); ?></a>
		</div>
	</div>
</section>