<?php if(have_rows('cta')): ?>

<section class="cta-blocks">
	
	<?php
		$ctaCount = 0;
		while(have_rows('cta')): the_row();
		$ctaCount++;
	?>

	<div class="row content-block-container">
		
		<?php if($ctaCount % 2 == 0): ?>
			
			<?php if(get_sub_field('cta_image')): ?>
			
			<div class="large-8 medium-8 columns">
				<div class="v-center">

					<div class="block-title">
						<h5 class="block-secondary-title"><?php the_sub_field('cta_title'); ?></h5>
					</div>
			  		<?php the_sub_field('cta_content'); ?>
			  		
			  		<?php if(get_sub_field('cta_link_text')): ?>
			  		<a class="button" href="<?php the_sub_field('cta_link_url'); ?>"><?php the_sub_field('cta_link_text'); ?></a>
			  		<?php endif; ?>

				</div>
			</div>	
			<div class="large-4 medium-4 columns">
				<img src="<?php the_sub_field('cta_image'); ?>"/>
			</div>
			
			<?php else: ?>
			
			<div class="small-12 columns">
				
				<div class="block-title">
					<h5 class="block-secondary-title"><?php the_sub_field('cta_title'); ?></h5>
				</div>
		  		<?php the_sub_field('cta_content'); ?>
		  		
		  		<?php if(get_sub_field('cta_link_text')): ?>
		  		<a class="button" href="<?php the_sub_field('cta_link_url'); ?>"><?php the_sub_field('cta_link_text'); ?></a>
		  		<?php endif; ?>
			  						
			</div>
			
			<?php endif; ?>
			
		<?php else: ?>
			
			<?php if(get_sub_field('cta_image')): ?>
			
			<div class="large-4 medium-4 columns">
				<img src="<?php the_sub_field('cta_image'); ?>"/>
			</div>	
			<div class="large-8 medium-8 columns">
				<div class="v-center">
					
					<div class="block-title">
						<h5 class="block-secondary-title"><?php the_sub_field('cta_title'); ?></h5>
					</div>
			  		<?php the_sub_field('cta_content'); ?>
	
			  		<?php if(get_sub_field('cta_link_text')): ?>
			  		<a class="button" href="<?php the_sub_field('cta_link_url'); ?>"><?php the_sub_field('cta_link_text'); ?></a>
			  		<?php endif; ?>
			  		
				</div>
			</div>
			
			<?php else: ?>
			
			<div class="small-12 columns">
				
				<div class="block-title">
					<h5 class="block-secondary-title"><?php the_sub_field('cta_title'); ?></h5>
				</div>
		  		<?php the_sub_field('cta_content'); ?>
		  		
		  		<?php if(get_sub_field('cta_link_text')): ?>
		  		<a class="button" href="<?php the_sub_field('cta_link_url'); ?>"><?php the_sub_field('cta_link_text'); ?></a>
		  		<?php endif; ?>
			  						
			</div>
			
			<?php endif; ?>
			
		<?php endif; ?>
		
	</div>

	<?php endwhile; ?>

</section>

<?php endif; ?>