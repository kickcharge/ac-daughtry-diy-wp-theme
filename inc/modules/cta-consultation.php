<section class="consultation">

	<div class="row">
		<div class="block-title">
			<h5 class="block-secondary-title"><?php the_field('consultation_cta_text', 'option'); ?></h5>
		</div>
		<a href="tel:9733353981" class="button"><?php the_field('phone_number', 'option'); ?></a>
	</div>

</section>