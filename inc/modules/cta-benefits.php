<section id="company-intro">

	<div class="row content-block-container">
		<div class="large-4 medium-4 columns">
			<img src="<?php the_field('benefits_cta_image', 'option'); ?>"/>
		</div>	
		<div class="large-8 medium-8 columns">
			<div class="v-center">
				<div class="block-title">
					<h4 class="block-primary-title"><?php the_field('benefits_cta_primary_text', 'option'); ?></h4>
					<h5 class="block-secondary-title"><?php the_field('benefits_cta_secondary_text', 'option'); ?></h5>
				</div>
	  			
	  			<?php the_field('benefits_cta_content', 'option'); ?>

	  			<a class="button" href="<?php the_field('benefits_cta_button_url', 'option'); ?>"><?php the_field('benefits_cta_button_text', 'option'); ?></a>
			</div>
		</div>
	</div>	

</section>