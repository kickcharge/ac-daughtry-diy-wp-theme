<section class="page-header">
	<div class="row">
		<div class="large-6 medium-6 columns">
			<div class="page-header-block-content">
				
				<div class="page-header-block-title">
					<h4 class="page-header-primary-title"><?php the_field('page_header_primary_title'); ?></h4>
					<h5 class="page-header-secondary-title"><?php the_field('page_header_secondary_title'); ?></h5>
				</div>
				
				<?php the_field('page_header_content'); ?>

<!-- 				<?php if(have_rows('page_header_contact_numbers')): ?>
				<ul class="contact-details">
					<?php while(have_rows('page_header_contact_numbers')): the_row(); ?>
					<li><span><?php the_sub_field('page_header_contact_number_title'); ?></span> <?php the_sub_field('page_header_contact_number'); ?></li>
					<?php endwhile; ?>
				</ul>
				<?php endif; ?>

				<div class="contact-hours row">

					<?php if(get_field('page_header_weekday_hours')): ?>
					<div class="large-6 columns">
						<?php the_field('page_header_weekday_hours'); ?>
					</div>
					<?php endif; ?>

					<?php if(get_field('page_header_weekend_hours')): ?>
					<div class="large-6 columns">
						<?php the_field('page_header_weekend_hours'); ?>
					</div>
					<?php endif; ?>

				</div> -->

			</div>
		</div>
		<div class="large-6 medium-6 columns page-header-image">
			<img src="<?php the_field('page_header_image'); ?>"/>
		</div>		
	</div>
</section>