<?php

	if ( ! function_exists('portfolio') ) {

	// Register Custom Post Type
	function portfolio() {

		$labels = array(
			'name'                => _x( 'Portfolio', 'Post Type General Name', 'gdstheme' ),
			'singular_name'       => _x( 'Portfolio', 'Post Type Singular Name', 'gdstheme' ),
			'menu_name'           => __( 'Portfolio', 'gdstheme' ),
			'name_admin_bar'      => __( 'Portfolio', 'gdstheme' ),
			'parent_item_colon'   => __( 'Parent Item:', 'gdstheme' ),
			'all_items'           => __( 'All Portfolio Items', 'gdstheme' ),
			'add_new_item'        => __( 'Add New Portfolio Item', 'gdstheme' ),
			'add_new'             => __( 'Add New', 'gdstheme' ),
			'new_item'            => __( 'New Portfolio Item', 'gdstheme' ),
			'edit_item'           => __( 'Edit Portfolio Item', 'gdstheme' ),
			'update_item'         => __( 'Update Portfolio Item', 'gdstheme' ),
			'view_item'           => __( 'View Portfolio Item', 'gdstheme' ),
			'search_items'        => __( 'Search Portfolio', 'gdstheme' ),
			'not_found'           => __( 'Not found', 'gdstheme' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'gdstheme' ),
		);
		$args = array(
			'label'               => __( 'portfolio', 'gdstheme' ),
			'description'         => __( 'This section is dedicated to managing your portfolio.', 'gdstheme' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'thumbnail', 'revisions', ),
			'taxonomies'          => array( 'category', 'post_tag' ),
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-format-gallery',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		register_post_type( 'portfolio', $args );

	}

	// Hook into the 'init' action
	add_action( 'init', 'portfolio', 0 );

	}

?>