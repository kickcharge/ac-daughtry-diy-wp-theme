<?php

if ( ! function_exists('coupons') ) {

// Register Custom Post Type
function coupons() {

	$labels = array(
		'name'                => _x( 'Coupon', 'Post Type General Name', 'gdstheme' ),
		'singular_name'       => _x( 'Coupons', 'Post Type Singular Name', 'gdstheme' ),
		'menu_name'           => __( 'Coupons', 'gdstheme' ),
		'name_admin_bar'      => __( 'Coupons', 'gdstheme' ),
		'parent_item_colon'   => __( 'Parent Item:', 'gdstheme' ),
		'all_items'           => __( 'All Coupon Items', 'gdstheme' ),
		'add_new_item'        => __( 'Add New Coupon Item', 'gdstheme' ),
		'add_new'             => __( 'Add New', 'gdstheme' ),
		'new_item'            => __( 'New Coupon Item', 'gdstheme' ),
		'edit_item'           => __( 'Edit Coupon Item', 'gdstheme' ),
		'update_item'         => __( 'Update Coupon Item', 'gdstheme' ),
		'view_item'           => __( 'View Coupon Item', 'gdstheme' ),
		'search_items'        => __( 'Search Coupons', 'gdstheme' ),
		'not_found'           => __( 'Not found', 'gdstheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'gdstheme' ),
	);
	$args = array(
		'label'               => __( 'Coupons', 'gdstheme' ),
		'description'         => __( 'This section is dedicated to managing your coupons.', 'gdstheme' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', 'revisions', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-tickets-alt',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'coupons', $args );

}
add_action( 'init', 'coupons', 0 );

}

?>