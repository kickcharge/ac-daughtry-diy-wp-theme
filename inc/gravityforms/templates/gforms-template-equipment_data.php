  <?php	  
	// This file is pulled in from our conditional statement located in: inc/gravityforms/combined/gforms-combined_passdata.php
	// Thhis pulls our ACF content from our CPT and puts them in columns
	// $cpt->ID is defined in: inc/gravityforms/combined/gforms-combined_passdata.php
	
	// Query the CPT here and only pull published posts
	// Create an array of posts
	// Below we also only target specific taxonomy
	
	$equipment_id = $cpt->ID;
	$equipment_title = get_the_title( $cpt->ID );
	$equipment_cost = get_post_meta( $cpt->ID, 'price', true );
	//$equipment_details = get_the_content();
	
?>
   
	   <div id="post-<?php echo $equipment_id; ?>" class="equipment-details">
		   
			<script type="text/javascript">
				
				jQuery(document).bind('gform_post_render', function(){
										
					var options = {
						hashTracking: 'false',
						closeOnOutsideClick: 'false'
					};

					jQuery('[data-remodal-id=equipment-modal-<?php echo $equipment_id; ?>]').remodal(options);

				});
				
			</script>
		   
		   <div class="large-6 columns">
			   
			   <?php echo '<span class="package-title">' . $equipment_title . '</span>:' . " " . '<span class="package-cost">' . $equipment_cost . '</span>'; ?>
			   
		   </div><!-- /.large-6 -->
		   
		   <div class="large-6 columns">
			   
			   <span class="package-learnmore"><a href="#equipment-modal-<?php echo $equipment_id; ?>">Learn More ></a></span>
			   
		   </div><!-- /.large-6 -->
		   
			<div class="remodal" data-remodal-id="package-modal-<?php echo $equipment_id; ?>">

			  <button data-remodal-action="close" class="remodal-close"></button>

			  <strong><?php echo $equipment_title; ?></strong>
			  
			  <?php //echo $equipment_details; ?>

			</div><!-- /.reveal -->
		   
	   </div><!-- /.post-class -->