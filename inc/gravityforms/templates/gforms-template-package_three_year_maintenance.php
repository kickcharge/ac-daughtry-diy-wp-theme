  <?php	  
	// This file is pulled in from our conditional statement located in: inc/gravityforms/combined/gforms-combined_passdata.php
	// Thhis pulls our ACF content from our CPT and puts them in columns
	// $cpt->ID is defined in: inc/gravityforms/combined/gforms-combined_passdata.php
	
	// Query the CPT here and only pull published posts
	// Create an array of posts
	// Below we also only target specific taxonomy
	
	$package_id = $cpt->ID;
	$package_title = get_the_title( $cpt->ID );
	$package_cost_one_year = get_post_meta( $cpt->ID, 'equipment_one_year_contract_price', true );
	$package_maintenance_three_year = get_post_meta( $cpt->ID, 'monitoring_plan_three_year_contract_price', true );
	
?>
   		   
<strong><?php echo $package_title; ?> - <?php echo $package_maintenance_three_year; ?></strong>