  <?php	  
	// This file is pulled in from our conditional statement located in: inc/gravityforms/combined/gforms-combined_passdata.php
	// Thhis pulls our ACF content from our CPT and puts them in columns
	// $cpt->ID is defined in: inc/gravityforms/combined/gforms-combined_passdata.php
	
	// Query the CPT here and only pull published posts
	// Create an array of posts
	// Below we also only target specific taxonomy
	
	$package_id = $cpt->ID;
	$package_title = get_the_title( $cpt->ID );
	$package_cost_one_year = get_post_meta( $cpt->ID, 'equipment_one_year_contract_upfront_price', true );
	$package_cost_three_year = get_post_meta( $cpt->ID, 'equipment_three_year_contract_upfront_price', true );
	$package_details = get_post_meta( $cpt->ID, 'learn_more_package_details', true ); // to format this output proper call: echo apply_filters( 'the_content', $package_details );
	
?>
   
	   <div id="post-<?php echo $package_id; ?>" class="package-details">
		   		   		   
			<script type="text/javascript">
				
				jQuery(document).bind('gform_post_render', function(){
										
					var options = {
						hashTracking: 'false',
						closeOnOutsideClick: 'false'
					};

					jQuery('[data-remodal-id=package-modal-<?php echo $package_id; ?>]').remodal(options);

				});
				
			</script>
		   
		   <div class="large-6 columns">
			   
			   <?php echo '<span class="package-title">' . $package_title . '</span>:' . " " . '<span class="package-cost three-year active">' . $package_cost_three_year . '</span>' . '<span class="package-cost one-year">' . $package_cost_one_year . '</span>'; ?>
			   
		   </div><!-- /.large-6 -->
		   
		   <div class="large-6 columns">
			   
			   <span class="package-learnmore"><a href="#package-modal-<?php echo $package_id; ?>">Learn More ></a></span>
			   
		   </div><!-- /.large-6 -->
		   
			<div class="remodal" data-remodal-id="package-modal-<?php echo $package_id; ?>">

			  <button data-remodal-action="close" class="remodal-close"></button>

			  <strong><?php echo $package_title; ?></strong>
			  
			  <?php echo apply_filters( 'the_content', $package_details ); ?>

			</div><!-- /.reveal -->
		   
	   </div><!-- /.post-class -->