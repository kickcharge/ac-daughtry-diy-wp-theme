<?php

// This file adds global functions for Gravity Forms
// You can edit the accordion_field function to change how the elements in the form are pressented
// This also has the 'get_package_data' function to retrieve package title, cost and details

function gform_unit_get_label( $cpt ) {
	// Create function to style the output of the data
	// Pass the cpt data post object	
	
	
	ob_start();
	
	// Use include(locate_template)) instead of get_template_part so that we can pass variables
	// This file has our HTML markup and data from ACF / Gravity Forms
	include(locate_template( 'inc/gravityforms/templates/gforms-template-package_data.php' ) );
	
	return ob_get_clean();

}

function gform_unit_get_label_three_year_maintenance( $cpt ) {
	// Create function to style the output of the data
	// Pass the cpt data post object	
	
	
	ob_start();
	
	// Use include(locate_template)) instead of get_template_part so that we can pass variables
	// This file has our HTML markup and data from ACF / Gravity Forms
	include(locate_template( 'inc/gravityforms/templates/gforms-template-package_three_year_maintenance.php' ) );
	
	return ob_get_clean();

}

function gform_unit_get_label_one_year_maintenance( $cpt ) {
	// Create function to style the output of the data
	// Pass the cpt data post object	
	
	
	ob_start();
	
	// Use include(locate_template)) instead of get_template_part so that we can pass variables
	// This file has our HTML markup and data from ACF / Gravity Forms
	include(locate_template( 'inc/gravityforms/templates/gforms-template-package_one_year_maintenance.php' ) );
	
	return ob_get_clean();

}

function gform_equipment_get_label( $cpt ) {
	// Create function to style the output of the data
	// Pass the cpt data post object	
	
	
	ob_start();
	
	// Use include(locate_template)) instead of get_template_part so that we can pass variables
	// This file has our HTML markup and data from ACF / Gravity Forms
	include(locate_template( 'inc/gravityforms/templates/gforms-template-equipment_data.php' ) );
	
	return ob_get_clean();

}

function get_package_data( $post_id ) {
	
	// These variables populate data on Gravity Forms product field
	
	$package_title = get_the_title( $post_id );
	$package_cost_one_year = get_post_meta( $post_id, 'equipment_one_year_contract_upfront_price', true );
	$package_cost_three_year = get_post_meta( $post_id, 'equipment_three_year_contract_upfront_price', true );
	$package_monitoring_one_year = get_post_meta( $post_id, 'monitoring_plan_one_year_contract_price', true );
	$package_monitoring_three_year = get_post_meta( $post_id, 'monitoring_plan_three_year_contract_price', true );
	$package_details = get_post_meta( $post_id, 'learn_more_package_details', true ); // to format this output proper call: echo apply_filters( 'the_content', $package_details );
	
	$data = array(
		'title' => $package_title,
		'cost_one_year' => $package_cost_one_year,
		'cost_three_year' => $package_cost_three_year,
		'monitoring_one_year' => $package_monitoring_one_year,
		'monitoring_three_year' => $package_monitoring_three_year,
		'details' => $package_details
	);
	return $data;
}

function get_equipment_data( $post_id ) {
	
	// These variables populate data on Gravity Forms product field
	
	$equipment_title = get_the_title( $post_id );
	$equipment_cost = get_post_meta( $post_id, 'price', true );
	
	$data = array(
		'title' => $equipment_title,
		'cost' => $equipment_cost,
	);
	return $data;

}

// Function to debug data to console

function debug_to_console( $data, $context = 'Debug in Console' ) {

    // Buffering to solve problems frameworks, like header() in this and not a solid return.
    ob_start();

    $output  = 'console.info( \'' . $context . ':\' );';
    $output .= 'console.log(' . json_encode( $data ) . ');';
    $output  = sprintf( '<script>%s</script>', $output );

    echo $output;
}

// DOUBLE CHECK THIS FUNCTION TO GET IT BY CLASS INSTEAD OF FORM ID


// Wrap my CPT fields within an accordion
// Documentation: https://www.gravityhelp.com/documentation/article/gform_field_container/
add_filter( 'gform_field_container_4_2', 'cpt_accordion_field', 10, 6 );
function cpt_accordion_field( $field_container, $field, $form, $css_class, $style, $field_content ) {
	
	if(IS_ADMIN) return $field_container; // only modify HTML on the front end
	
	$accordion_html .= '<li id="field_4_' . $field->id  . '" class="' . $css_class . '">';
		$accordion_html .= '<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">';
			$accordion_html .= '<li class="accordion-title is-active" data-accordion-item>';
				$accordion_html .= '<a href="#" class="accordion-title"><span>' . $field->label . '</span></a>';
				$accordion_html .= '<div class="accordion-content" data-tab-content>';
					$accordion_html .= '<span class="package-description">' . $field->description . '</span>';
						$accordion_html .= '<div class="ginput_container ginput_container_radio">';
							$accordion_html .= '<ul class="gfield_radio">';
								$accordion_html .= $field->get_radio_choices( $value, $disabled_text, $form_id );
							$accordion_html .= '</ul>';
						$accordion_html .= '</div>';
				$accordion_html .= '</div>';
			$accordion_html .= '</li>';
		$accordion_html .= '</ul>';
	$accordion_html .= '</li>';
			
    return $accordion_html;
    
}

add_filter( 'gform_field_container_4_4', 'cpt_accordion_field_2', 10, 6 );
function cpt_accordion_field_2( $field_container, $field, $form, $css_class, $style, $field_content ) {
	
	if(IS_ADMIN) return $field_container; // only modify HTML on the front end
	
	$accordion_html .= '<li id="field_4_' . $field->id  . '" class="' . $css_class . '">';
		$accordion_html .= '<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">';
			$accordion_html .= '<li class="accordion-title" data-accordion-item>';
				$accordion_html .= '<a href="#" class="accordion-title"><span>' . $field->label . '</span></a>';
				$accordion_html .= '<div class="accordion-content" data-tab-content>';
					$accordion_html .= '<span class="package-description">' . $field->description . '</span>';
						$accordion_html .= '<div class="ginput_container ginput_container_radio">';
							$accordion_html .= '<ul class="gfield_radio">';
								$accordion_html .= $field->get_radio_choices( $value, $disabled_text, $form_id );
							$accordion_html .= '</ul>';
						$accordion_html .= '</div>';
				$accordion_html .= '</div>';
			$accordion_html .= '</li>';
		$accordion_html .= '</ul>';
	$accordion_html .= '</li>';
			
    return $accordion_html;
    
}

// Documentation: https://www.gravityhelp.com/documentation/article/gform_field_input/
// Documentation: https://www.gravityhelp.com/documentation/article/field-object/
// Wrap fields in accordion
// To activate you must give the field a class of package-accordion

add_filter( 'gform_field_input', 'accordion_field', 10, 5 );
function accordion_field( $content, $field, $value, $lead_id, $form_id ) {
	
	if(IS_ADMIN) return $content; // only modify HTML on the front end
	
    if ( $field->cssClass == 'package-accordion' ) {
        
        if ( RG_CURRENT_VIEW == 'entry' ) {
            $mode = empty( $_POST['screen_mode'] ) ? 'view' : $_POST['screen_mode'];
           
            if ( $mode == 'view' ) {
           
                $content = '<tr>
                                <td colspan="2" class="entry-view-section-break">'. esc_html( GFCommon::get_label( $field ) ) . '</td>
                            </tr>';
            } else {

                $content= "<tr valign='top'>
                        <td class='detail-view'>
                            <div style='margin-bottom:10px; border-bottom:1px dotted #ccc;'><h2 class='detail_gsection_title'>" . esc_html( GFCommon::get_label( $field ) ) . "</h2></div>
                        </td>
                    </tr>";
            }
            
        } else {

            $delete_field_link = "<a class='field_delete_icon' id='gfield_delete_{$field->id}' title='" . __( 'click to delete this field', 'gravityforms' ) . "' href='javascript:void(0);' onclick='StartDeleteField(this);'>" . __( 'Delete', 'gravityforms' ) . "</a>";
            $delete_field_link = apply_filters( 'gform_delete_field_link', $delete_field_link );

            //The edit and delete links need to be added to the content (if desired), when using this hook
            $admin_buttons = IS_ADMIN ? $delete_field_link . " <a class='field_edit_icon edit_icon_collapsed' title='" . __( 'click to edit this field', 'gravityforms' ) . "'>" . __( 'Edit', 'gravityforms' ) . "</a>" : "";
			
			$content .= '<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">';
				$content .= '<li class="accordion-title is-active" data-accordion-item>';
		            $content .= '<a href="#" class="accordion-title is-active"><span>' . $field->label . '</span></a>';
		            $content .= '<div class="accordion-content" data-tab-content>';
						$content .= '<span class="package-description">' . $field->description . '</span>';
						$content .= '<div class="ginput_container ginput_container_radio">';
							$content .= '<ul class="gfield_radio">';
								$content .= $field->get_radio_choices( $value, $disabled_text, $form_id );
							$content .= '</ul>';
						$content .= '</div>';
					$content .= '</div>';
				$content .= '</li>';
			$content .= '</ul>';
        }

    }
    
    return $content;
}

// Dynamic field population
add_filter( 'gform_field_value_variableagreement', 'my_custom_population_function' );
function my_custom_population_function( $value ) {



  	
}

?>