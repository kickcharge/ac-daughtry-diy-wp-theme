<?php

// This file allows us to run conditions on specfic fields within our form
// To keep it cleaned up, queries and templates for how the data is presented are stored in separate files

// Allow for shortcodes in form fields, descriptions & labels
add_filter( 'gform_get_form_filter_4', 'shortcode_unautop', 11 );
add_filter( 'gform_get_form_filter_4', 'do_shortcode', 11 );

// Set gform_pre_render filter specifically on our quote form
// Documentation: https://www.gravityhelp.com/documentation/article/gform_pre_render/
add_filter( 'gform_pre_render_4', 'cpt_load_data' );

function cpt_load_data( $form ) {

    foreach ( $form['fields'] as &$field ) {
    
        if ( $field->id == 2 ) { // Choose Package Field
	        
			include(locate_template( 'inc/gravityforms/queries/query-packages.php' ) );
		
		} elseif ( $field->id == 7 ) { // Three Year Maintenance
			
			include(locate_template( 'inc/gravityforms/queries/query-packages_maintenance_three.php' ) );
			
		} elseif ( $field->id == 8 ) { // One Year Maintenance
			
			include(locate_template( 'inc/gravityforms/queries/query-packages_maintenance_one.php' ) );
		
		} elseif ( $field->id == 4 ) { // Add Equipment Field

			include(locate_template( 'inc/gravityforms/queries/query-equipment.php' ) );
		
		}
		
	}

	return $form;

}

?>