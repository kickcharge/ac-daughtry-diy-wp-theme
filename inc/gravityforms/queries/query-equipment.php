<?php
	
	// Get all posts from 'equipment' CPT
	// Query the CPT here and only pull published posts
	// Create an array of posts

	$cpt = new WP_Query(array(
		'post_type' => 'equipment',
		'posts_per_page' => -1,
        'order' => 'ASC',
        'supress_filters' => false
	));

	$choices = array();
    // Loop through each of these CPT posts, and build our choices array

	$cpt = $cpt->get_posts();
	foreach( $cpt as $cpt ) {

		$equipment_data = get_equipment_data( $cpt->ID );
		$choices[] = array(
            // We can now pull any field we want from our CPT post, by setting it below (ACF included)
            'text'	=> gform_equipment_get_label( $cpt ), // This links to our inc/gravityforms/gforms-quote_functions.php file
			'value' => $cpt->post_title,
			'price'	=> $equipment_data['cost'],
            // We want the user to select the appropriate choice. Set to true if otherwise.
			'isSelected' => false
		);
	}

    $field->cssClass .= ' gforms-cpt-data'; // Add a unique CSS class to the field specifically
	$field->choices = $choices;
	
?>