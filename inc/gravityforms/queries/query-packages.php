<?php
	
	// Get all posts from 'package' CPT
	// Query the CPT here and only pull published posts
	// Create an array of posts

	$cpt = new WP_Query(array(
		'post_type' => 'package',
		'posts_per_page' => -1,
        'order' => 'ASC',
        'supress_filters' => false
	));

	$choices = array();
    // Loop through each of these CPT posts, and build our choices array

	$cpt = $cpt->get_posts();
	foreach( $cpt as $cpt ) {

		$package_data = get_package_data( $cpt->ID );
		$choices[] = array(
            // We can now pull any field we want from our CPT post, by setting it below (ACF included)
            'text'	=> gform_unit_get_label( $cpt ), // This links to our inc/gravityforms/gforms-quote_functions.php file
			'value' => $cpt->post_title,
			'price'	=> $package_data['cost_three_year'], // set in gforms-functions.php file
			'three_year_base_price' => $package_data['cost_three_year'],
			'one_year_base_price' => $package_data['cost_one_year'],
			'one_year_monitoring' => $package_data['monitoring_one_year'],
			'three_year_monitoring' => $package_data['monitoring_three_year'],
            // We want the user to select the appropriate choice. Set to true if otherwise.
			'isSelected' => false
		);
		
	}

    $field->cssClass .= ' gforms-cpt-data'; // Add a unique CSS class to the field specifically
	$field->choices = $choices;

	debug_to_console($choices);
	
?>