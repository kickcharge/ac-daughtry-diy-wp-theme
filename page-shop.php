<?php
	/* Template Name: Shop Page */
	get_header();
?>

<section class="divider"></section>

<section>
	<div class="row">
		<div class="large-7 medium-7 columns">
			
			<?php
				if(have_posts()):
					while(have_posts()): the_post();
			?>
				<?php the_content(); ?>
			<?php
					endwhile;
				endif;
			?>

		</div>
		<div class="large-4 medium-4 columns">
			<div id="cart-overview">
				
				<div class="row">
					<div class="column">
						<a href="#">Save</a> <a href="#">Print</a> <a href="#">Go Back</a>
					</div>
				</div>

				<div class="row">
					<div class="column">
						<h5>Cart Summary</h5>
					</div>
				</div>

				<div class="row">
					<div class="small-6 columns">
						<p>Monitoring Cost:</p>
					</div>
					<div class="small-6 columns">
						<p id="cart-monitoring-cost"><strong>Please Choose a Package</strong></p>
					</div>
					<div class="small-6 columns">
						<p>Equipment Cost:</p>
					</div>
					<div class="small-6 columns">
						<p id="cart-equipment-cost"><strong>Please Choose a Package</strong></p>
					</div>
					<div class="small-6 columns">
						<p>Shipping:</p>
					</div>
					<div class="small-6 columns">
						<p id="cart-shipping-cost"><strong>Free!</strong></p>
					</div>					
				</div>

			</div>
		</div>
	</div>
</section>

<section class="divider"></section>

<?php get_template_part('inc/modules/cta-faqs'); ?>

<?php get_footer(); ?>