<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section class="divider"></section>

<?php get_template_part('inc/modules/cta-blocks'); ?>

<?php if(have_rows('features')): ?>
<section id="equipment-features">
	<div class="row">

	<?php

		$featureCount = 12 / count(get_field('features'));

		$classes = "large-" . $featureCount . " medium-" . $featureCount;

		while(have_rows('features')): the_row();

	?>
		<div class="columns <?php echo $classes; ?>">
			<h5><?php the_sub_field('feature_title'); ?></h5>
			<?php the_sub_field('feature_content'); ?>
		</div>
	<?php endwhile; ?>

	</div>
</section>
<?php endif; ?>

<?php if(get_field('specs_pdf')): ?>
<section id="equipment-specs">
	<div class="row">
		<div class="block-title">
			<h5 class="block-secondary-title">Specs</h5>
			<p>Interested in the product details? You can download them <a href="<?php the_field('specs_pdf'); ?>" target="_blank" rel="nofollow">here.</a></p>
		</div>
	</div>
</section>
<?php endif; ?>

<?php get_template_part('inc/modules/cta-faqs'); ?>

<?php if(get_field('product_video')): ?>
<section id="product-feature-video">
	<div class="row">
		<div class="responsive-embed">
			<?php the_field('product_video'); ?>
		</div>
	</div>
</section>
<?php endif; ?>

<section id="product-feature-purchase-options">
	<div class="row">
		<div class="block-title">
			<h5 class="block-secondary-title">Purchase as part of your package</h5>
			<p>Or, if you are a current customer you may buy this product if you are logged in.</p>
		</div>
		<div class="text-center">
			<a href="<?php echo bloginfo('url'); ?>/packages/" class="button">See Packages</a>
			<a href="<?php echo bloginfo('url'); ?>/shop/" class="button">Buy Now</a>
		</div>
	</div>
</section>

<?php endwhile; ?>

<?php get_footer(); ?>