<?php
	get_header();
?>

<section class="divider"></section>

<section>
	<div class="row">
		<div class="large-7 medium-7 columns">

			<form method="post" action="" id="buy-now">
				<ul class="accordion" data-accordion>

				  <li class="accordion-item is-active" data-accordion-item>
				    <a href="#" class="accordion-title">Choose Your Package</a>
				    <div class="accordion-content" data-tab-content>
				    	<?php
				    		
				    		$packageOptions = array(
				    			'post_type' => 'package',
				    			'posts_per_page' => -1,
				    		);

				    		$packages = new WP_Query($packageOptions);

				    		if($packages->have_posts()):
				    			while($packages->have_posts()): $packages->the_post();
				    	?>
				    	<div class="package">
				    		
				    		<input name="package-option" type="radio" value="package-id-<?php the_ID(); ?>" />
				    		
				    		<label><?php the_title(); ?> <a href="<?php the_permalink(); ?>" target="_blank" rel="nofollow">Learn More ></a></label>

				    		<input type="hidden" name="package-id-<?php the_ID(); ?>-monitoring-plan-one-year-contract-price" value="<?php the_field('monitoring_plan_one_year_contract_price'); ?>"/>
				    		<input type="hidden" name="package-id-<?php the_ID(); ?>-monitoring-plan-three-year-contract-price" value="<?php the_field('monitoring_plan_three_year_contract_price'); ?>"/>

				    		<input type="hidden" name="package-id-<?php the_ID(); ?>-one-year-contract-equipment-price" value="<?php the_field('equipment_one_year_contract_price'); ?>"/>
				    		<input type="hidden" name="package-id-<?php the_ID(); ?>-three-year-contract-equipment-price" value="<?php the_field('equipment_three_year_contract_price'); ?>"/>				    		

				    	</div>
				    	<?php
				    		
				    			endwhile;
				    		endif;
				    		
				    		wp_reset_postdata();

				    	?>
				    </div>
				  </li>

				  <li class="accordion-item" data-accordion-item>
				    <a href="#" class="accordion-title">Choose Your Monitoring Plan</a>
				    <div class="accordion-content" data-tab-content>
				    	<div class="">
				    		<input name="package-option-monitoring" type="radio" value="3 Year" checked/>
				    		<label>3 Year monitoring agreement.</label>
				    	</div>
				    	<div class="package-option">
				    		<input name="package-option-monitoring" type="radio" value="1 Year"/>
				    		<label>1 Year monitoring agreement.</label>
				    	</div>
				    </div>
				  </li>

				  <li class="accordion-item" data-accordion-item>
				    <a href="#" class="accordion-title">Choose Your Equipment</a>
				    <div class="accordion-content" data-tab-content>
				    	<?php

				    		$equipmentArgs = array(
				    			'post_type' => 'equipment',
				    			'posts_per_page' => -1
				    		);

				    		$equipment = new WP_Query($equipmentArgs);

				    		if($equipment->have_posts()):
				    			while($equipment->have_posts()): $equipment->the_post();

				    	?>
				    	<div class="package-equipment-option row" data-product-id="<?php the_ID(); ?>">
				    		<div class="column large-2 medium-2">
				    			<img src="https://placehold.it/320x320&text=Product Image"/>
				    		</div>
				    		<div class="column large-8 medium-8">
				    			<label><?php the_title(); ?></label>
				    		</div>
				    		<div class="column large-2 medium-2">
				    			<input type="text" class="equipment-qty" name="product-<?php the_ID(); ?>-quanity" value="1"/>
				    		</div>
				    	</div>
				    	<?php

				    			endwhile;
				    		endif;

				    		wp_reset_postdata();

				    	?>
				    </div>
				  </li>

				  <li class="accordion-item" data-accordion-item>
				    <a href="#" class="accordion-title">Shipping Method</a>
				    <div class="accordion-content" data-tab-content>
				    	<div class="package-option">
				    		<input name="package-option-shipping" type="radio" value="0" checked/>
				    		<label>FedEx Ground Shipping - <strong>Free!</strong> - 4-6 Business Days</label>
				    	</div>
				    	<div class="package-option">
				    		<input name="package-option-shipping" type="radio" value="50"/>
				    		<label>FedEx Overnight Shipping - <strong>Add $50.00</strong> - 4-6 Business Days</label>
				    	</div>
				    </div>
				  </li>				  	

				</ul>

				<a href="#" class="button">Continue</a>

			</form>
		</div>
		<div class="large-4 medium-4 columns">
			<div id="cart-overview">
				
				<div class="row">
					<div class="column">
						<a href="#">Save</a> <a href="#">Print</a> <a href="#">Go Back</a>
					</div>
				</div>

				<div class="row">
					<div class="column">
						<h5>Cart Summary</h5>
					</div>
				</div>

				<div class="row">
					<div class="small-6 columns">
						<p>Monitoring Cost:</p>
					</div>
					<div class="small-6 columns">
						<p id="cart-monitoring-cost"><strong>Please Choose a Package</strong></p>
					</div>
					<div class="small-6 columns">
						<p>Equipment Cost:</p>
					</div>
					<div class="small-6 columns">
						<p id="cart-equipment-cost"><strong>Please Choose a Package</strong></p>
					</div>
					<div class="small-6 columns">
						<p>Shipping:</p>
					</div>
					<div class="small-6 columns">
						<p id="cart-shipping-cost"><strong>Free!</strong></p>
					</div>					
				</div>

			</div>
		</div>
	</div>
</section>

<section class="divider"></section>

<?php get_template_part('inc/modules/cta-faqs'); ?>

<?php get_footer(); ?>

<script type="text/javascript">
	
	jQuery(document).ready(function($){

		$('input[name="package-option"]').on('change', function(){
			
			packageId = $(this).val();

			threeYearMonitoringPricing = $('input[name="' + packageId +'-monitoring-plan-three-year-contract-price"]').val();
			oneYearMonitoringPricing = $('input[name="' + packageId +'-monitoring-plan-one-year-contract-price"]').val();

			threeYearEquipmentPricing = $('input[name="' + packageId +'-three-year-contract-equipment-price"]').val();
			oneYearEquipmentPricing = $('input[name="' + packageId +'-one-year-contract-equipment-price"]').val();			

			if($('input[name="package-option-monitoring"]:checked').val() == '3 Year') {
				
				$('#cart-monitoring-cost').html('<strong>' + threeYearMonitoringPricing + '</strong>');
				$('#cart-equipment-cost').html('<strong>' + threeYearEquipmentPricing + '</strong>');

			} else {

				$('#cart-monitoring-cost').html('<strong>' + oneYearMonitoringPricing + '</strong>');
				$('#cart-equipment-cost').html('<strong>' + oneYearEquipmentPricing + '</strong>');

			}

		});

		$('input[name="package-option-monitoring"]').on('change', function(){

			if($(this).val() == '3 Year') {
				
				$('#cart-monitoring-cost').html('<strong>' + threeYearMonitoringPricing + '</strong>');
				$('#cart-equipment-cost').html('<strong>' + threeYearEquipmentPricing + '</strong>');

			} else {

				$('#cart-monitoring-cost').html('<strong>' + oneYearMonitoringPricing + '</strong>');
				$('#cart-equipment-cost').html('<strong>' + oneYearEquipmentPricing + '</strong>');

			}

		});

		$('input[name="package-option-shipping"]').on('change', function(){

			if($(this).val() !== '0') {
				
				$('#cart-shipping-cost').html('<strong>' + $(this).val() + '</strong>');

			} else {

				$('#cart-shipping-cost').html('<strong>Free!</strong>');

			}

		});

	});

</script>