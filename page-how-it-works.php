<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php get_template_part('inc/modules/page-header'); ?>

<section class="divider"></section>

<section class="base-styles">
	<div class="row ">
		<div class="column">
			<?php the_content(); ?>
		</div>
	</div>
</section>

<section id="company-intro">

	<div class="row content-block-container">
		<div class="large-4 medium-4 columns">
			<img src="<?php echo bloginfo('template_directory'); ?>/img/benefits-img.jpg"/>
		</div>	
		<div class="large-8 medium-8 columns">
			<div class="v-center">
				<div class="block-title">
					<h4 class="block-primary-title">BENEFITS</h4>
					<h5 class="block-secondary-title">Feel safe while you save.</h5>
				</div>
	  			<p>By choosing to buy your DIY system from A.C. Daughtry, you don’t just save money. You enjoy sophisticated, up-to-date equipment and the added comfort of working with the local professionals you can trust.</p>
	  			<a class="button" href="<?php echo bloginfo('url'); ?>/company/benefits/">Learn More</a>
			</div>
		</div>
	</div>	

</section>

<?php endwhile; ?>

<?php get_footer(); ?>